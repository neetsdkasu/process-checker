VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Begin VB.Form CheckProcessMainForm 
   Caption         =   "CheckProcess"
   ClientHeight    =   2145
   ClientLeft      =   60
   ClientTop       =   630
   ClientWidth     =   5385
   LinkTopic       =   "Form1"
   ScaleHeight     =   2145
   ScaleWidth      =   5385
   StartUpPosition =   2  '画面の中央
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  '下揃え
      Height          =   255
      Left            =   0
      TabIndex        =   3
      Top             =   1890
      Width           =   5385
      _ExtentX        =   9499
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   5080
            MinWidth        =   5080
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'なし
      Caption         =   "Frame1"
      Height          =   1335
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3495
      Begin VB.ListBox List2 
         Height          =   1140
         Left            =   1320
         TabIndex        =   2
         Top             =   0
         Width           =   1935
      End
      Begin VB.ListBox List1 
         Height          =   1140
         Left            =   0
         Sorted          =   -1  'True
         TabIndex        =   1
         Top             =   0
         Width           =   1335
      End
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   3600
      Top             =   0
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "メニュー(&M)"
      Begin VB.Menu mnuMenuFront 
         Caption         =   "常に最前面に表示(&A)"
      End
      Begin VB.Menu mnuMenuBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuMenuExit 
         Caption         =   "終了(&X)"
      End
   End
End
Attribute VB_Name = "CheckProcessMainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Call Timer1_Timer
    Timer1.Enabled = True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Timer1.Enabled = False
    If mnuMenuFront.Checked Then
        SetTopMostWindow Me.hwnd, False
    End If
End Sub

Private Sub Form_Resize()
    Static blnMini  As Boolean
    If Me.WindowState = vbMinimized Then
        If mnuMenuFront.Checked Then
            blnMini = True
            SetTopMostWindow Me.hwnd, False
        End If
        Exit Sub
    End If
    If blnMini Then
        blnMini = False
        If mnuMenuFront.Checked Then
            SetTopMostWindow Me.hwnd, True
        End If
    End If
    
    Dim msw!, msh!
    msw = Me.ScaleWidth
    msh = Me.ScaleHeight - StatusBar1.Height
    msw = IIf(msw < 500, 500, msw)
    msh = IIf(msh < 500, 500, msh)
    Frame1.Visible = False
    Frame1.Width = msw
    Frame1.Height = msh
    List1.Width = Int(msw * 0.475)
    List1.Height = msh
    List1.ListIndex = List1.ListCount - 1
    List2.Left = List1.Width
    List2.Width = msw - List1.Width
    List2.Height = msh
    List2.ListIndex = 0
    Frame1.Visible = True
End Sub

Private Sub mnuMenuExit_Click()
    Unload Me
End Sub

Private Sub mnuMenuFront_Click()
    With mnuMenuFront
        .Checked = Not .Checked
        If .Checked Then
            SetTopMostWindow Me.hwnd, True
        Else
            SetTopMostWindow Me.hwnd, False
        End If
    End With
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next

    ' プロセス情報取得開始
    Dim lngHandle As Long
    lngHandle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0)

    ' プロセス情報取得
    Dim lngResult As Long
    Dim procEntry As PROCESSENTRY32
    procEntry.Size = SIZEOF_PROCESSENTRY32
    lngResult = Process32First(lngHandle, procEntry)

    Do While lngResult
        ' ＰＩＤとプロセス名の一覧表示
        Dim strFname As String
        Dim lngPid As Long
        Dim blnFlag As Boolean
        Dim i As Long
        strFname = Left(procEntry.FileName, InStr(procEntry.FileName, vbNullChar) - 1)
        lngPid = procEntry.ProcessID
        blnFlag = False
        For i = 0 To List1.ListCount - 1
            If List1.ItemData(i) = lngPid Then
                blnFlag = True
                Exit For
            End If
        Next i
        If blnFlag = False Then
            List1.AddItem Format(procEntry.ProcessID, "00000") & _
                          String(2, " ") & _
                          strFname
            List1.ItemData(List1.NewIndex) = -1 * procEntry.ProcessID
            List1.ListIndex = List1.NewIndex
            List2.AddItem "+ " & List1.List(List1.ListIndex), 0
            List2.ListIndex = 0
            StatusBar1.Panels.Item(1).Text = "プロセス数 " & CStr(List1.ListCount)
        Else
            List1.ItemData(i) = -1 * procEntry.ProcessID
        End If

        ' 次プロセス情報取得
        lngResult = Process32Next(lngHandle, procEntry)
    Loop
    
    For i = List1.ListCount - 1 To 0 Step -1
        If List1.ItemData(i) > 0 Then
            List2.AddItem "- " & List1.List(i), 0
            List2.ListIndex = 0
            List1.RemoveItem i
            StatusBar1.Panels.Item(1).Text = "プロセス数 " & CStr(List1.ListCount)
        Else
            List1.ItemData(i) = -1 * List1.ItemData(i)
        End If
    Next i
    
    Do While (List2.ListCount > 200)
        List2.RemoveItem List2.ListCount - 1
    Loop

    ' プロセス情報取得終了
    CloseHandle lngHandle
End Sub


