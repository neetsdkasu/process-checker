Attribute VB_Name = "CheckProcessModule"
Option Explicit

' �v���Z�X�ꗗ�`�o�h
Public Const TH32CS_SNAPHEAPLIST = 1
Public Const TH32CS_SNAPPROCESS = 2
Public Const TH32CS_SNAPTHREAD = 4
Public Const TH32CS_SNAPMODULE = 8
Public Const TH32CS_SNAPALL = 15
Public Const SIZEOF_PROCESSENTRY32 As Long = 296
Public Type PROCESSENTRY32
    Size As Long
    RefCount As Long
    ProcessID As Long
    HeapID As Long
    ModuleID As Long
    ThreadCount As Long
    ParentProcessID As Long
    BasePriority As Long
    Flags As Long
    FileName As String * 260
End Type
Public Declare Function CreateToolhelp32Snapshot Lib "kernel32" ( _
    ByVal Flags As Long, ByVal ProcessID As Long) As Long
Public Declare Function Process32First Lib "kernel32" ( _
    ByVal lngHandleshot As Long, _
    ByRef ProcessEntry As PROCESSENTRY32) As Long
Public Declare Function Process32Next Lib "kernel32" ( _
    ByVal lngHandle As Long, _
    ByRef ProcessEntry As PROCESSENTRY32) As Long
' �v���Z�X�̋N���^�I���`�o�h
'Public Declare Function OpenProcess Lib "kernel32" ( _
'    ByVal dwDesiredAccess As Long, _
'    ByVal bInheritHandle As Long, _
'    ByVal dwProcessId As Long) As Long
Public Declare Function CloseHandle Lib "kernel32" ( _
    ByVal hObject As Long) As Long
'Public Declare Function TerminateProcess Lib "kernel32" ( _
'    ByVal hProcess As Long, _
'    ByVal uExitCode As Long) As Long
Public Const SYNCHRONIZE       As Long = &H100000
Public Const PROCESS_TERMINATE As Long = &H1


