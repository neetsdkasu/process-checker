VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "WindowHandleの取得"
   ClientHeight    =   7500
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6750
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7500
   ScaleWidth      =   6750
   StartUpPosition =   3  'Windows の既定値
   Begin VB.CommandButton Command3 
      Caption         =   "ProcessID順"
      Enabled         =   0   'False
      Height          =   615
      Left            =   3000
      TabIndex        =   3
      Top             =   120
      Width           =   1335
   End
   Begin VB.CommandButton Command2 
      Caption         =   "hWnd順"
      Enabled         =   0   'False
      Height          =   615
      Left            =   1560
      TabIndex        =   2
      Top             =   120
      Width           =   1335
   End
   Begin VB.ListBox List1 
      Height          =   6540
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   6495
   End
   Begin VB.CommandButton Command1 
      Caption         =   "hWndの取得"
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1335
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mc1WindowInfo() As Class1

Private Sub SetWindowInfo()
    Dim colWindowInfo   As Collection
    Dim c&, i&, v
    Set colWindowInfo = GetWindowInfo()
    c = colWindowInfo.Count
    ReDim mc1WindowInfo(c - 1)
    i = 0
    For Each v In colWindowInfo
        Set mc1WindowInfo(i) = v
        i = i + 1
    Next v
End Sub

Private Sub SortHWND()
    Dim i&, j&, c&, v
    c = UBound(mc1WindowInfo)
    For i = 0 To c
        For j = c To i + 1 Step -1
            If mc1WindowInfo(j).hWnd < mc1WindowInfo(j - 1).hWnd Then
                Set v = mc1WindowInfo(j)
                Set mc1WindowInfo(j) = mc1WindowInfo(j - 1)
                Set mc1WindowInfo(j - 1) = v
            End If
        Next j
    Next i
End Sub

Private Sub SortProcessID()
    Dim i&, j&, c&, v
    c = UBound(mc1WindowInfo)
    For i = 0 To c
        For j = c To i + 1 Step -1
            If mc1WindowInfo(j).ProcessID < mc1WindowInfo(j - 1).ProcessID Then
                Set v = mc1WindowInfo(j)
                Set mc1WindowInfo(j) = mc1WindowInfo(j - 1)
                Set mc1WindowInfo(j - 1) = v
            End If
        Next j
    Next i
End Sub

Private Sub ListUp()
    Dim i&, c&
    c = UBound(mc1WindowInfo)
    List1.Clear
    For i = 0 To c
        With mc1WindowInfo(i)
            List1.AddItem CStr(.hWnd) & vbTab & CStr(.ProcessID) & vbTab & .Text
        End With
    Next i
End Sub

Private Sub Command1_Click()
    Command1.Enabled = False
    Command2.Enabled = False
    Command3.Enabled = False
    Call SetWindowInfo
    Call ListUp
    Command1.Enabled = True
    Command2.Enabled = True
    Command3.Enabled = True
End Sub

Private Sub Command2_Click()
    Call SortHWND
    Call ListUp
End Sub

Private Sub Command3_Click()
    Call SortProcessID
    Call ListUp
End Sub

