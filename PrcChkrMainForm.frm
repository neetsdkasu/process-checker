VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Begin VB.Form PrcChkrMainForm 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "ProccessChecker"
   ClientHeight    =   4710
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   7215
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4710
   ScaleWidth      =   7215
   StartUpPosition =   2  '画面の中央
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   120
      Top             =   4800
   End
   Begin MSComctlLib.ListView ListView2 
      Height          =   1935
      Left            =   0
      TabIndex        =   1
      Top             =   2760
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   3413
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "PID"
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "WND"
         Object.Width           =   1411
      EndProperty
   End
   Begin MSComctlLib.ListView ListView3 
      Height          =   1935
      Left            =   2520
      TabIndex        =   2
      Top             =   2760
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   3413
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Date"
         Object.Width           =   3175
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "State"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "PID"
         Object.Width           =   1411
      EndProperty
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   2775
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   4895
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Date"
         Object.Width           =   3176
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Status"
         Object.Width           =   1589
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "PID"
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "HWND"
         Object.Width           =   1589
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Key             =   "text"
         Text            =   "Text"
         Object.Width           =   4233
      EndProperty
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "メニュー(&M)"
      Begin VB.Menu mnuMenuFront 
         Caption         =   "常に手前に表示(&A)"
      End
      Begin VB.Menu mnuMenuBar 
         Caption         =   "-"
      End
      Begin VB.Menu mnuMenuExit 
         Caption         =   "終了(&X)"
      End
   End
End
Attribute VB_Name = "PrcChkrMainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mpifWindowInfo()    As PrcInfoClass
Private mdicWindowInfo      As Scripting.Dictionary
Private mdicWindowInfoBF    As Scripting.Dictionary
Private mdicProccess        As Scripting.Dictionary
Private mdicProccessBF      As Scripting.Dictionary
Private mcolChangeProccess  As Collection
Private mdicChangePID       As Scripting.Dictionary

Private Sub SetWindowInfo()
    Dim c&, v, w, k$
    Set mdicWindowInfoBF = mdicWindowInfo
    Set mdicProccessBF = mdicProccess
    Set mdicProccess = New Scripting.Dictionary
    Set mdicWindowInfo = GetWindowInfo()
    
    Dim pif1    As PrcInfoClass
    
    v = mdicWindowInfo.Items()
    
    For Each w In v
        Set pif1 = w
        k = "P" & Format(pif1.ProcessID, "00000")
        If mdicProccess.Exists(k) Then
            c = mdicProccess.Item(k)
        Else
            c = 0
        End If
        c = c + 1
        mdicProccess.Item(k) = c
    Next
    
End Sub

Private Sub CheckProccess()
    Set mcolChangeProccess = New Collection
    Set mdicChangePID = New Scripting.Dictionary
    
    Dim pif1    As PrcInfoClass
    Dim v, w, c&
    
    'First Access
    If mdicWindowInfoBF Is Nothing Then
        v = mdicWindowInfo.Keys()
        For Each w In v
            Set pif1 = mdicWindowInfo.Item(w)
            pif1.Status = cpifStayed
            mcolChangeProccess.Add pif1
        Next
        v = mdicProccess.Keys()
        For Each w In v
            mdicChangePID.Add w, mdicProccess.Item(w)
        Next
        
        Exit Sub
    End If
    
    
    'Check Finished Proccess(hwnd)
    v = mdicWindowInfoBF.Keys()
    For Each w In v
        If mdicWindowInfo.Exists(w) = False Then
            Set pif1 = mdicWindowInfoBF.Item(w)
            pif1.Status = cpiffinished
            mcolChangeProccess.Add pif1
        End If
    Next

    'Check Starteted Proccess(hwnd)
    v = mdicWindowInfo.Keys()
    For Each w In v
        If mdicWindowInfoBF.Exists(w) = False Then
            Set pif1 = mdicWindowInfo.Item(w)
            pif1.Status = cpifstarted
            mcolChangeProccess.Add pif1
        End If
    Next
    
    'Check Finished Proccess(pid)
    v = mdicProccessBF.Keys()
    For Each w In v
        If mdicProccess.Exists(w) = False Then
            mdicChangePID.Add w, 0
        End If
    Next
    
    'Check Started Proccess(pid) & Change WNDs
    v = mdicProccess.Keys()
    For Each w In v
        c = CLng(mdicProccess.Item(w))
        If mdicProccessBF.Exists(w) = False Then
            mdicChangePID.Add w, c
        Else
            If CLng(c) <> CLng(mdicProccessBF.Item(w)) Then
                mdicChangePID.Add w, -c
            End If
        End If
    Next
    

End Sub

Private Sub ListUpProccess(strTime As String)
    Dim pif1    As PrcInfoClass
    Dim lsi1    As ListItem
    Dim w, v, s, c&, i&, p&
    s = Array("Unknown", "Stayed", "Finished", "Started")
    
    'ListUp
    For Each w In mcolChangeProccess
        Set pif1 = w
        Set lsi1 = ListView1.ListItems.Add(Index:=1, Text:=strTime)
        With lsi1
            .ListSubItems.Add Text:=s(pif1.Status)
            .ListSubItems.Add Text:=CStr(pif1.ProcessID)
            .ListSubItems.Add Text:=CStr(pif1.hwnd)
            .ListSubItems.Add(Text:=pif1.Text).ToolTipText = pif1.Text
        End With
    Next
    
    'History limit 100
    Do While (ListView1.ListItems.Count > 100)
        ListView1.ListItems.Remove 101
    Loop
    
    'ProccessIDs
    v = mdicChangePID.Keys()
    For Each w In v
        c = mdicChangePID.Item(w)
        p = CLng(Replace(w, "P", "0"))
        If c = 0 Then
            i = ListView2.ListItems(w).Index
            ListView2.ListItems.Remove i
            Set lsi1 = ListView3.ListItems.Add(Index:=1, Text:=strTime)
            With lsi1
                .ListSubItems.Add Text:=s(cpiffinished)
                .ListSubItems.Add Text:=p
            End With
        ElseIf c < 0 Then
            c = Abs(c)
            ListView2.ListItems(w).SubItems(1) = c
        Else
            ListView2.ListItems.Add(Index:=1, Key:=w, Text:=p).ListSubItems.Add Text:=c
            Set lsi1 = ListView3.ListItems.Add(Index:=1, Text:=strTime)
            With lsi1
                .ListSubItems.Add Text:=s(cpifstarted)
                .ListSubItems.Add Text:=p
            End With
        End If
    Next
    
    'History limit 20
    Do While (ListView3.ListItems.Count > 20)
        ListView3.ListItems.Remove 21
    Loop
    
End Sub

Private Sub ShowProccess()
    
    Dim strTime     As String
    
    strTime = CStr(Now)
    
    Call SetWindowInfo
    Call CheckProccess
    Call ListUpProccess(strTime)
    

End Sub

Private Sub Form_Load()
    Call ShowProccess
    Timer1.Enabled = True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Timer1.Enabled = False
    If mnuMenuFront.Checked Then
        SetTopMostWindow Me.hwnd, False
    End If
End Sub


Private Sub Form_Resize()
    Static blnMini  As Boolean
    If Me.WindowState = vbMinimized Then
        If mnuMenuFront.Checked Then
            blnMini = True
            SetTopMostWindow Me.hwnd, False
        End If
        Exit Sub
    End If
    If blnMini Then
        blnMini = False
        If mnuMenuFront.Checked Then
            SetTopMostWindow Me.hwnd, True
        End If
    End If
End Sub

Private Sub mnuMenuExit_Click()
    Unload Me
End Sub

Private Sub mnuMenuFront_Click()
    With mnuMenuFront
        .Checked = Not .Checked
        If .Checked Then
            SetTopMostWindow Me.hwnd, True
        Else
            SetTopMostWindow Me.hwnd, False
        End If
    End With
End Sub

Private Sub Timer1_Timer()
    Call ShowProccess
End Sub
