VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PrcInfoClass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public hWnd         As Long
Public ProcessID    As Long
Public Text         As String
Public Key          As String
Public Status       As String

Public Enum PrcInfoStatusConst
    cpifUnknown = 0
    cpifStayed = 1
    cpiffinished = 2
    cpifstarted = 3
End Enum
