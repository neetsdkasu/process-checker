Attribute VB_Name = "PrcChkrModule"
Option Explicit

Private Declare Function EnumWindows Lib "user32" ( _
            ByVal lpEnumFunc As Long, _
            ByVal lParam As Long) As Boolean

Private Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" ( _
            ByVal hWnd As Long, _
            ByVal lpString As String, _
            ByVal cch As Long) As Long

Private Declare Function GetWindowThreadProcessId Lib "user32" ( _
            ByVal hWnd As Long, _
            ByRef lpdwProcessId As Long) As Long


Private mcolHWNDs As Collection
Private mdicWInfo As Scripting.Dictionary

Public Function GetWindowInfo() As Scripting.Dictionary
    Call DoEnumWindows
    Set GetWindowInfo = mdicWInfo
End Function

Private Sub GetInfo()
    Dim v       As Variant
    Dim s       As String
    Dim hWnd    As Long
    Dim pId     As Long
    Dim n       As Long
    Dim info    As PrcInfoClass
    Set mdicWInfo = New Scripting.Dictionary
    For Each v In mcolHWNDs
        hWnd = CLng(v)
        s = String(1024, vbNullChar)
        n = GetWindowText(hWnd, s, 1024)
        If n > 0 Then
            s = Left(s, InStr(1, s, vbNullChar) - 1)
        Else
            s = ""
        End If
        GetWindowThreadProcessId hWnd, pId
        Set info = New PrcInfoClass
        With info
            .hWnd = hWnd
            .ProcessID = pId
            .Text = s
            .Status = cpifUnknown
            .Key = CStr(.ProcessID) & "::" & CStr(.hWnd)
        End With
        mdicWInfo.Add info.Key, info
    Next v
End Sub

Private Sub DoEnumWindows()
    Set mcolHWNDs = New Collection
    Call EnumWindows(AddressOf EnumWindowsProc, 0)
    Call GetInfo
    Set mcolHWNDs = Nothing
End Sub


Public Function EnumWindowsProc( _
            ByVal hWnd As Long, ByVal lParam As Long) As Boolean
                
    'Debug.Print "HWND "; hWnd
    mcolHWNDs.Add hWnd
    EnumWindowsProc = True
End Function

